var models = require('../models');
var Video = models.Video;

exports.getVideoById = function(id,callback){
	Video.findOne({id:id},callback);
};

exports.newMovie = function(id,vtype,title,image,intro,rating,duration,pubtime,director,actor,area,type,tags,sites,callback){
	var video = new Video();
	video.id = id;
	video.vtype = vtype;
	video.title = title;
	video.image = image;
	video.intro = intro;
	video.rating = rating;
	video.duration = duration;
	video.pubtime = pubtime;
	video.director=director;
	video.actor = actor;
	video.area = area;
	video.type = type;
	video.tags = tags;
	video.sites = sites;
	video.save(callback);
}

