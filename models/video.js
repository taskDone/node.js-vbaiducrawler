var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VideoSchema = new Schema({
	id: Number,
	vtype:{type:Number,default:1},
	title: String,
	image: String,
	intro: String,
	rating: Number,
	duration: String,
	pubtime: String,
	director: [String],
	actor: [String],
	area: [String],
	type: [String],
	tags: [String],
	sites:[],
	create_at:{type:Date,default:Date.now}
});

VideoSchema.index({id:-1});

mongoose.model('Video',VideoSchema);

