var mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1/node_video', function (err) {
  if (err) {
    console.error('connect to %s error: ','127.0.0.1', err.message);
    process.exit(1);
  }
});

require('./video');

exports.Video = mongoose.model('Video');
